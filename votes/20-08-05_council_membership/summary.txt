=====================================================================
Summary
=====================================================================

Topic: Community Council Membership
Date taken: 20/7/22 - 20/8/5
Vote count: 17 (of 101, 17%)

Secretary: Karsten Loesing

Topic put to a vote were...

  Question 1: List up to five of these individuals you'd like to see
  on the new Community Council.

  * Alexander Færøy (ahf)
  * Antonela Debiasi (antonela)
  * Cecylia Bocovich (cohosh)
  * Gaba (gaba)

  Question 2: Would you be uncomfortable having any of these people
  adjudicate your issues?

Results were...

  * The next council will be composed of Alexander, Antonela,
    Cecylia, Gaba.

=====================================================================
Votes
=====================================================================

Al Smith
Alexander Færøy
Allen Gunn
Antonela Debiasi
David Goulet
Gaba
Georg Koppen
gus
intrigeri
kat
Linus Nordberg
Matt Traudt
Nick Mathewson
Philipp Winter
Rob Jansen
Steven Murdoch
Taylor Yu

1060;ahf,antonela,cohosh,gaba;<none>
6659;ahf,antonela,gaba;<none>
7765;ahf,antonela,cohosh,gaba;<none>
7788;ahf,antonela,cohosh,gaba;<none>
8947;ahf,antonela,cohosh,gaba;<none>
9437;ahf,antonela,cohosh,gaba;<none>
10611;ahf,antonela,cohosh,gaba;<abstain>
18345;ahf,antonela,cohosh,gaba;<none>
20670;ahf,antonela,cohosh,gaba;<none>
22371;ahf,antonela,cohosh,gaba;<none>
24153;ahf,antonela,cohosh,gaba;<none>
24670;ahf,antonela,cohosh,gaba;<none>
25583;ahf,antonela,cohosh,gaba;<none>
26338;ahf,antonela,cohosh,gaba;<none>
28165;ahf,antonela,cohosh,gaba;<abstain>
30863;ahf;<none>
32635;ahf,antonela,cohosh,gaba;<none>

=====================================================================
Question 1: List up to five of these individuals you'd like to see
on the new Community Council.
=====================================================================

This question had one abstention.

ahf: 17 (100%)
antonela: 16 (94%)
cohosh: 15 (88%)
gaba: 16 (94%)

---------------------------------------------------------------------
Result:
---------------------------------------------------------------------

The next council will be composed of Alexander, Antonela, Cecylia,
and Gaba.

=====================================================================
Question 2: Would you be uncomfortable having any of these people
adjudicate your issues?
=====================================================================

This question had ten abstentions.

ahf: 0 (0%)
antonela: 0 (0%)
cohosh: 0 (0%)
gaba: 0 (0%)

---------------------------------------------------------------------
Result:
---------------------------------------------------------------------

Nobody reached 25% so everyone's eligible to be on the council.

