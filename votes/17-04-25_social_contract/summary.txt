=====================================================================
Summary
=====================================================================

Topic: Social Contract
Date taken: 17/4/11 - 17/4/25
Vote count: 27 (of 84 eligible voters, 8 opt-outs, 36%)

Secretary: Karsten Loesing
Proposer: Alison Macrina

Topic put to a vote were...

  Question 1: Social Contract Proposal

    There are no alternate proposals for these guidelines, so please choose
    A or B.

    A. I approve of the attached proposal.
    B. I reject the attached proposal.

Results were...

  * Proposal passes. 96% approved of its adoption. We'll institute
    this policy into our community bylaws.

=====================================================================
Votes
=====================================================================

Alison
Allen Gunn
Arthur D. Edelstein
Colin Childs
Damian Johnson
David Goulet
Gabriella Coleman
Griffin Boyce
Ian Goldberg
isabela
Joshua Gay
Kate
Kathleen Brade
Linda Naeun Lee
Linus Nordberg
Mark Smith
Meredith Hoban Dunn
micah
Nick Mathewson
Paul Syverson
Rob Jansen
Silvia [Hiro]
Sina Rabbani
Steven Murdoch
Sue Gardner
Tom Ritter
Yawning Angel

14; A
187; A
3141; A
3172; A
3370; A
3595; A
6395; B
6742; A
8695; A
9273; A
9347; A
9350; A
10428; A
10589; A
13254; A
15649; A
17820; A
19107; A
20700; A
25915; A
26925; A
30154; A
30377; A
30426; A
31390; A
32079; A
32766; A

=====================================================================
Question 1: Social Contract Proposal
=====================================================================

Approve: 26 (96%)
Reject: 1 (4%)

---------------------------------------------------------------------
Result:
---------------------------------------------------------------------

Proposal passes. 96% approved of its adoption. We'll institute
this policy into our community bylaws.

