# The Tor Project Social Contract

At The Tor Project, we make tools that help promote and protect the essential
human rights of people everywhere. We have a set of guiding principles that
make that possible, but for a long time, those principles were more or less
unspoken. In order to ensure that project members build a Tor that reflects the
commitment to our ideals, we've taken a cue from our friends at Debian and
written the Tor Social Contract -- the set of principles that show who we are
and why we make Tor.

Our social contract is a set of behaviors and goals: not just the promised
results we want for our community, but the ways we seek to achieve them. We
want to grow Tor by supporting and advancing these guidelines in the time we
are working on Tor, while taking care not to undermine them in the rest of our
time.

The principles can also be used to help recognize when people's actions or
intents are hurting Tor. Some of these principles are established norms; things
we've been doing every day for a long time; while others are more aspirational
-- but all of them are values we want to live in public, and we hope they will
make our future choices easier and more open. This social contract is one of
several documents that define our community standards, so if you're looking for
things that aren't here (e.g. something that might be in a code of conduct)
bear in mind that they might exist, in a different document.

Social goals can be complex. If there is ever tension in the application of the
following principles, we will always strive to place highest priority on the
safety and freedom of any who would use the fruits of our endeavors. The social
contract can also help us work through such tensions -- for example, there are
times when we might have a need to use tools that are not completely open
(contradicting point 2) but opening them would undermine our users' safety
(contradicting point 6). Using such a tool should be weighed against how much
it's needed to make our technologies usable (point 1). And if we do use such a
tool, we must be honest about its capabilities and limits (point 5).

Tor is not just software, but a labor of love produced by an international
community of people devoted to human rights. This social contract is a promise
from our internal community to the rest of the world, affirming our commitment
to our beliefs. We are excited to present it to you.

## 1. We advance human rights by creating and deploying usable anonymity and privacy technologies.

We believe that privacy, the free exchange of ideas, and access to information
are essential to free societies. Through our community standards and the code
we write, we provide tools that help all people protect and advance these
rights.

## 2. Open and transparent research and tools are key to our success.

We are committed to transparency; therefore, everything we release is open and
our development happens in the open. Whenever feasible, we will continue to
make our source code, binaries, and claims about them open to independent
verification. In the extremely rare cases where open development would
undermine the security of our users, we will be especially vigilant in our peer
review by project members.

## 3. Our tools are free to access, use, adapt, and distribute.

The more diverse our users, the less is implied about any person by simply
being a Tor user. This diversity is a fundamental goal and we aim to create
tools and services anyone can access and use. Someone's ability to pay for
these tools or services should not be a determining factor in their ability to
access and use them. Moreover, we do not restrict access to our tools unless
access is superceded by our intent to make users more secure.

We expect the code and research we publish will be reviewed and improved by
many different people, and that is only possible if everyone has the ability to
use, copy, modify, and redistribute this information. We also design, build,
and deploy our tools without collecting identifiable information about our
users.

## 4. We make Tor and related technologies ubiquitous through advocacy and education.

We are not just people who build software, but ambassadors for online freedom.
We want everybody in the world to understand that their human rights --
particularly their rights to free speech, freedom to access information, and
privacy -- can be preserved when they use the Internet. We teach people how and
why to use Tor and we are always working to make our tools both more secure and
more usable, which is why we use our own tools and listen to user feedback. Our
vision of a more free society will not be accomplished simply behind a computer
screen, and so in addition to writing good code, we also prioritize community
outreach and advocacy.

## 5. We are honest about the capabilities and limits of Tor and related technologies.

We never intentionally mislead our users nor misrepresent the capabilities of
the tools, nor the potential risks associated with using them. Every user
should be free to make an informed decision about whether they should use a
particular tool and how they should use it. We are responsible for accurately
reporting the state of our software, and we work diligently to keep our
community informed through our various communication channels.

## 6. We will never intentionally harm our users.

We take seriously the trust our users have placed in us. Not only will we
always do our best to write good code, but it is imperative that we resist any
pressure from adversaries who want to harm our users. We will never implement
front doors or back doors into our projects. In our commitment to transparency,
we are honest when we make errors, and we communicate with our users about our
plans to improve.

