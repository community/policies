# Tor Core Community Voting Bylaws

Decision making within the Tor Project typically happens by [rough
consensus](https://en.wikipedia.org/wiki/Rough_consensus). When proposing,
amending, or ratifying community governance documents (including this voting
procedure), the process is as follows.

## How do I make a proposal?

* Announce your proposal to tor-project@ or tor-internal@. The email
  subject should start with '[Proposal]'.

* Your proposal will enter a 'discussion phase' for at least a week.
  During this time the proposal can be refined based on feedback and
  alternate proposals put forward.

  There is no limit on the duration of the discussion phase. The
  proposer can take as long as they'd like to reach a proposal
  they think will have broad appeal.

* To move forward with a vote the proposal must receive at least two
  other sponsors. In addition, any alternate proposal with at least
  three supporters will also be included in the vote.

## How do we run the vote?

Votes are taken by an elected secretary and conducted as follows...

* The secretary calls for a vote by emailing tor-internal@ with
  the proposals being put forward. The email subject should start
  with '[Vote]'.

* The vote is conducted over the course of two weeks using either
  [Instant Runoff](https://en.wikipedia.org/wiki/Instant-runoff_voting) (aka
  [Alternate Vote](https://www.youtube.com/watch?v=3Y3jE3B8HsE)) or
  [Schulze-method Condorcet](https://en.wikipedia.org/wiki/Schulze_method). Prior
  to the vote the secretary will specify which. In either case to cast a vote
  list members rank available options along with one for 'take no action'.

  For example, say there were four proposals: A, B, C, D. You love B,
  like C, but hate A and D. Your vote might be...

  B, C, <none>, A, D

* Votes are submitted by emailing the secretary who will reply with
  a five digit number to act as their receipt. For reference a quick
  method to generate these can be...

  https://www.random.org/strings/?num=1&len=5&digits=on&format=plain

  Alternatively, to lessen work on the secretary they can set up
  voting software (like Civs, Devotee, etc) to aid in the process.

* Anyone on tor-internal@ may vote. Each person gets one vote.

* No late votes are accepted.

* Until the deadline votes may be changed.

* Voters are encouraged but not required to sign their vote with
  a key known by the secretary.

* Votes should be a simple ordering of the options listed. Vague
  or conditional votes may get a reply from the secretary asking
  for clarification.

* Proposal with the most votes wins. However, if 'take no action'
  accounts for at least 1/3 of the vote the proposals are rejected.

  In other words...

    X = Number of 'take no action' votes
    N = Number of votes, including abstentions

    The vote succeeds if:
    X < ceil(float(N)/3)

* The secretary cannot cast a vote. However, in the case of a tie
  they act as the tie breaker.

* Votes require a quorum consisting of a square root of eligible voters.

  In other words...

    N = Number of votes, including abstentions
    E = Number of eligible voters

    The vote succeeds if:
    N >= ceil(sqrt(float(E)))

* Votes are concluded by an email from the secretary with the
  results. This email includes everybody's votes, names replaced
  with their numeric token. It also includes a list of who voted.

* Ratified documents will reside in version control.

* Proposals take effect one week after the vote is concluded
  if there are no objections. If there are objections the
  proposal is suspended for two additional weeks for the
  results to be audited.

## Vote Secretary

The vote secretary is an elected position with no limit on their
term. At any point tor-internal@ members can call for a vote on a
new secretary. This is similar to a normal vote except...

* The 'discussion phase' is replaced with a one week call for
  volunteers. Any tor-internal@ member interested in having
  this responsibility can add themselves to the running.

* The existing secretary picks a temporary secretary who isn't
  in the running to conduct this vote. If at least two people
  veto this person another temporary secretary is selected.

  To prevent vetos from blocking the process indefinitely each
  subsequent candidate requires one more veto (ie, second
  candidate requires three vetos, the third four, etc).

* Otherwise this is a normal Instant Runoff vote conducted over
  the course of two weeks.

Secretaries cannot conduct a vote for their own proposal. To
recuse themselves the secretary picks another person to conduct
the vote using the veto rules described above.
